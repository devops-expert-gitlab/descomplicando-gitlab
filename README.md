# Descomplicando Gitlab

# Treinamento Descomplicando o GitLab criado ao vivo na Twitch

### Day - 1
- Entendemos o que é o Git
- Endentemos o que é o GitLab
- Como criar um grupo no GitLab
- Como criar um repositório no GitLab
- Aprendemos os comandos básicos para manipulação de arquivos e diretórios no git
- Como criar uma Branch
- Como criar um Merge Request
- Como adicionar um membro no projeto 
- Como fazer o merge na Master/Main
